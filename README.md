newsdiffs-chrome
================

NewsDiffs Chrome is a [Google Chrome](http://google.com/chrome) extension
for monitoring [NewsDiffs](http://newsdiffs.org) while you browse.

How it works
------------

NewsDiffs tracks changes in online news articles. In addition to showing lists
of changes, the website allows you to find articles by URL. This plugin makes
that process more transparent by checking for changes on supported pages in
the background, notifying you when the page you're reading has been changed:

![Screenshot](img/screenshot.png)

Clicking on the icon brings up the change log for that page in a new tab:

![Screenshot - log](img/log.png)

Currently supported sites
-------------------------

- [The New York Times](http://nytimes.com)
- [BBC](http://bbc.co.uk)
- [CNN](http://cnn.com)
- [The Washington Post](http://washingtonpost.com)
- [Politico](http://politico.com)

How to install
--------------

The extension is not on the Chrome Web Store yet, so you have to install it manually.

- Clone the repository: `git clone https://github.com/zthomae/newsdiffs-chrome.git`
- Go to the Chrome extensions page. Click "Load unpacked extension..."
- Select the newsdiffs-chrome directory.

To update the plugin, click "Reload" in the extensions's settings on the extensions page.